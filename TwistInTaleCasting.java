/**
	Exception in thread "main" java.lang.ClassCastException: 
	class BlackInk cannot be cast to class Printable 
	(BlackInk and Printable are in unnamed module of loader 'app')
	at TwistInTaleCasting.main(TwistInTaleCasting.java:17
*/

class Ink{}
interface Printable {}
class ColorInk extends Ink implements Printable {}
class BlackInk extends Ink{}

public class TwistInTaleCasting {
	public static void main(String args[]) {
		Printable printable = null;
		BlackInk blackInk = new BlackInk();
		printable = (Printable)blackInk;
	}
}