/**
Which of the following are valid Java identifiers? (Choose all that apply)
	*A. A$B
	*B. _helloWorld
	C. true
	D. java.lang
	*E. Public
	F. 1980_s
*/

public class Ch01Q01SG {
	public static void main(String[] args) {
		int A$B = 0;
		int _helloWorld = 0;
		//int true = 0;
		//int java.lang = 0;
		int Public = 0;
		//int 1980_s = 0;
	}
}