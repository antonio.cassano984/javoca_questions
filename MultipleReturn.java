/**
	Output:
	>>> (catch) returnedValue : 10
	>>> (finally) returnedValue : 20
	>>>  10
*/


public class MultipleReturn {
	
	int getInt() {
		int returnedValue = 10;
		try {
			String[] myArr = {"one", "two"};
			System.out.println(myArr[5]);
		} catch(Exception e) {
			System.out.println("(catch) returnedValue : " + returnedValue);
			return returnedValue;
		} finally {
			returnedValue += 10;
			System.out.println("(finally) returnedValue : " + returnedValue);
		}
		return returnedValue;
	}
	
	public static void main(String[] args) {
		System.out.println(new MultipleReturn().getInt());  // prints 10 not 20
	}
}