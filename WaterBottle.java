public class WaterBottle {
	private String brand;
	private boolean empty;
	public static void main(String[] args) {
		WaterBottle wb = new WaterBottle();
		System.out.print("Empty = " + wb.empty);
		System.out.print(", Brand = " + wb.brand);
} }

/**
What is the output of the following program?
A. Line 6 generates a compiler error.
B. Line 7 generates a compiler error.
C. There is no output.
*D. Empty = false, Brand = null
E. Empty = false, Brand =
F. Empty = null, Brand = null
*/

